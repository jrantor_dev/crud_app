<?php 
session_start();
require_once "pdo.php";
include "validate.php";

if(!isset($_SESSION['name'])){
	die("ACCESS DENIED");
}

if(isset($_POST['cancel'])){
	header("Location: index.php");
	return;
}

$make = "";
$model = "";
$year = "";
$mileage = "";
$success = "";

if(isset($_POST['update'])){

    if(!empty($_POST['make']) && !empty($_POST['model']) && !empty($_POST['year']) && !empty($_POST['mileage'])){

    	$make = input_check($_POST['make']);
        $model = input_check($_POST['model']);
    	$year = input_check($_POST['year']);
    	$mileage = input_check($_POST['mileage']);

    	if(!check_number($year)){
    		$_SESSION['error'] = "Year must be numeric";
    		header("Location: edit.php?autos_id=".$_POST['autos_id']);
    		return;
    	}

        if(!check_number($mileage)){
            $_SESSION['error'] = "Mileage must be numeric";
            header("Location: edit.php?autos_id=".$_POST['autos_id']);
            return;
        }

    }
    else{

    	 if(empty($_POST['make']) || empty($_POST['model']) || empty($_POST['year']) || empty($_POST['mileage'])) {
    		$_SESSION['error'] = "All fields are required!";
    		header("Location: edit.php?autos_id=".$_POST['autos_id']);
    		return;
    	}
    }

        $sql = "UPDATE autos SET make = :mk, model = :md, year = :yr, mileage = :ml WHERE auto_id = :autos_id";
        $stmt = $pdo->prepare($sql);
        $stmt->execute(array(
        ':mk' => $make,
        ':md' => $model,
        ':yr' => $year,
        ':ml' => $mileage,
        ':autos_id' => $_POST['autos_id']
    ));
        $_SESSION['success'] = "Record edited";
            header('Location: index.php');
            return;
}


if ( ! isset($_GET['autos_id']) ) {
  $_SESSION['error'] = "Missing autos_id";
  header('Location: index.php');
  return;
}

$stmt = $pdo->prepare("SELECT * FROM autos where auto_id = :xyz");
$stmt->execute(array(":xyz" => $_GET['autos_id']));
$row = $stmt->fetch(PDO::FETCH_ASSOC);
if ( $row === false ) {
    $_SESSION['error'] = 'Bad value for autos_id';
    header( 'Location: index.php' ) ;
    return;
}

$m = $row['make'];
$md = $row['model'];
$y = $row['year'];
$mi = $row['mileage'];
$auto_id = $row['auto_id'];

?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Janta Roy Antor</title>
	<link rel="stylesheet" href="css/bootstrap.min.css">
</head>
<body>
<div class="container">
	<h4>Tracking Autos for <?php echo $_SESSION['name'];  ?>  </h4>
	<span class="text text-danger">
	  <?php
	  if ( isset($_SESSION['error']) ) {
          echo('<p>'.htmlentities($_SESSION['error'])."</p>\n");
          unset($_SESSION['error']);}
	   ?>
	</span>
	<form action="" method="POST">
		<label for="make">Make:</label> <br>	
		<input type="text" name="make" value="<?php echo $m; ?>"> <br>

        <label for="model">Model:</label> <br>    
        <input type="text" name="model" value="<?php echo $md; ?>"> <br>

		<label for="year">Year:</label> <br>
		<input type="text" name="year" value="<?php echo $y; ?>"> <br>

		<label for="mileage">Mileage:</label> <br>
		<input type="text" name="mileage" value="<?php echo $mi; ?>"> <br> <br>

        <input type="hidden" name="autos_id" value="<?php echo $auto_id; ?>">

		<input type="submit" name="update" value="Save">

		<input type="submit" name="cancel" class="btn btn-light" value="Cancel">

	</form>

</div>	
</body>
</html>