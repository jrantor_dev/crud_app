<?php 
session_start();
require_once "pdo.php";
include "validate.php";

if(!isset($_SESSION['name'])){
	die("ACCESS DENIED");
}

if(isset($_POST['cancel'])){
	header("Location: index.php");
	return;
}

$make = "";
$model = "";
$year = "";
$mileage = "";
$success = "";

if(isset($_POST['add'])){

    if(!empty($_POST['make']) && !empty($_POST['model']) && !empty($_POST['year']) && !empty($_POST['mileage'])){

    	$make = input_check($_POST['make']);
        $model = input_check($_POST['model']);
    	$year = input_check($_POST['year']);
    	$mileage = input_check($_POST['mileage']);

    	if(!check_number($year)){
    		$_SESSION['error'] = "Year must be numeric";
    		header('Location: add.php');
    		return;
    	}

        if(!check_number($mileage)){
            $_SESSION['error'] = "Mileage must be numeric";
            header('Location: add.php');
            return;
        }

    }
    else{

    	 if(empty($_POST['make']) || empty($_POST['model']) || empty($_POST['year']) || empty($_POST['mileage'])) {
    		$_SESSION['error'] = "All fields are required!";
    		header('Location: add.php');
    		return;
    	}
    }


        $stmt = $pdo->prepare('INSERT INTO autos (make,model,year,mileage) VALUES (:mk,:md,:yr,:ml)');
        $stmt->execute(array(
        ':mk' => $make,
        ':md' => $model,
        ':yr' => $year,
        ':ml' => $mileage
    ));
        $_SESSION['success'] = "Record Added";
            header('Location: index.php');
            return;
}

?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Janta Roy Antor</title>
	<link rel="stylesheet" href="css/bootstrap.min.css">
</head>
<body>
<div class="container">
	<h4>Tracking Autos for <?php echo $_SESSION['name'];  ?>  </h4>
	<span class="text text-danger">
	  <?php
	  if ( isset($_SESSION['error']) ) {
          echo('<p>'.htmlentities($_SESSION['error'])."</p>\n");
          unset($_SESSION['error']);}
	   ?>
	</span>
	<form action="" method="POST">
		<label for="make">Make:</label> <br>
		<input type="text" name="make"> <br>

        <label for="model">Model:</label> <br>    
        <input type="text" name="model"> <br>

		<label for="year">Year:</label> <br>
		<input type="text" name="year"> <br>

		<label for="mileage">Mileage:</label> <br>
		<input type="text" name="mileage"> <br> <br>

		<input type="submit" name="add" value="Add">

		<input type="submit" name="cancel" class="btn btn-light" value="Cancel">

	</form>

</div>	
</body>
</html>