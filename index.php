<?php
require_once 'pdo.php';
session_start();

$stmt = $pdo->prepare('SELECT * FROM autos ORDER BY auto_id DESC');
$stmt->execute();
$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Janta Roy Antor</title>
	<link rel="stylesheet" href="css/bootstrap.min.css">
</head>
<body>
<div class="container">
	<h2>Welcome to the Automobiles Database</h2>
	<span class="text text-success">
	  <?php
	  if ( isset($_SESSION['success']) ) {
          echo('<p>'.htmlentities($_SESSION['success'])."</p>\n");
          unset($_SESSION['success']);}
	   ?>
	</span>

	<span class="text text-danger">
	  <?php
	  if ( isset($_SESSION['error']) ) {
          echo('<p>'.htmlentities($_SESSION['error'])."</p>\n");
          unset($_SESSION['error']);}
	   ?>
	</span>

	<?php 
	if(!isset($_SESSION['name'])){
		echo '<a href="login.php">Please log in</a>';
	    echo '<p>Attempt to <a href="add.php">add data</a> without logging in</p>';
	}

	else{

		if(sizeof($rows) <= 0){
			echo '<p>No rows found</p>';
		}

		else{
			echo ('<table class="table">');
			echo('<tr><th>');
			echo('Make</th><th>');
			echo('Model</th><th>');
			echo('Year</th><th>');
			echo('Mileage</th></tr>');
		foreach($rows as $row){
				echo('<tr><td>');
				echo($row['make']);
				echo('</td><td>');
				echo($row['model']);
				echo('</td><td>');
				echo($row['year']);
				echo('</td><td>');
				echo($row['mileage']);
				echo('</td><td>');
				echo('<a href="edit.php?autos_id='.$row['auto_id'].'">Edit</a> / ');
				echo('<a href="delete.php?autos_id='.$row['auto_id'].'">Delete</a>');
				echo('</td></tr>');
		}

			echo('</table>');

		}

		echo('<a href="add.php">Add New Entry</a> <br>');
		echo('<a href="logout.php">Logout</a>');
	}
	
	?>
</div>
</body>
</html>