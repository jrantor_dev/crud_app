<?php
session_start();
include "validate.php";

if(isset($_POST['cancel'])){
	header("Location: index.php");
	return;
}

$salt = 'XyZzy12*_';
$hash = hash('md5',$salt.'php123');

$email = "";

if(isset($_POST['email']) && isset($_POST['pass'])){
	if(!empty($_POST['email']) && !empty($_POST['pass'])){
		$check = hash('md5', $salt.$_POST['pass']);
		if(check_email($_POST['email']) && $check == $hash){
		 $email = input_check($_POST['email']);

         // Redirect the browser to view.php
         $_SESSION['name'] = $_POST['email'];
		 header("Location: index.php");
		 error_log("Login success ".$_POST['email']);
         return;
		}
		else{
			if(!check_email($_POST['email'])) {
				$_SESSION['error'] = "Email must have an at-sign (@)";
				header("Location: login.php");
				return;
			}
			if($check!=$hash) {
				$_SESSION['error'] = "Incorrect password!";
				error_log("Login fail ".$_POST['email']." $check");
				header("Location: login.php");
				return;
			}
			
		}  
	}

	else{
		$_SESSION['error'] = "Email and password are required";
		header("Location: login.php");
		return;
  
  }
	}
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Janta Roy Antor</title>
	<link rel="stylesheet" href="css/bootstrap.min.css">
</head>
<body>
<div class="container">
	<h4>Please Log In</h4>
	<span class="text text-danger"><?php 
	if ( isset($_SESSION['error']) ) {
          echo('<p style="color: red;">'.htmlentities($_SESSION['error'])."</p>\n");
          unset($_SESSION['error']);} ?>  
   </span>
	<form action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>" method="POST">
		<label for="email">User Name:</label> <br>	
		<input type="text" name="email" > <br>

		<label for="pass">Password:</label> <br>
		<input type="password" name="pass"> <br> <br><!-- password: php123 -->

		<input type="submit" name="Login" value="Log In">
		<input type="submit" name="cancel" value="Cancel">
	</form>

</div>	
</body>
</html>