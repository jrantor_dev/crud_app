<?php
echo "<title>Janta Roy Antor</title>";

 function input_check($data){
	$data = htmlentities($data);
	return $data;
}

 function check_email($email){
	return filter_var($email, FILTER_VALIDATE_EMAIL);
}

function check_text($text){
	return preg_match('/^[a-zA-Z\s]+$/',$text);
}

function check_number($number){
	return is_numeric($number);
}
?>