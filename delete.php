<?php 
session_start();
require_once "pdo.php";

if(isset($_POST['delete'])){
	$sql = "DELETE FROM autos WHERE auto_id = :a_id";
	$stmt = $pdo->prepare($sql);
	$stmt->execute(array(':a_id'=>$_POST['autos_id']));

	$_SESSION['success'] = 'Record deleted';
    header( 'Location: index.php' ) ;
    return;
}

if(isset($_POST['cancel'])){
	header("Location: index.php");
	return;
}

if ( ! isset($_GET['autos_id']) ) {
  $_SESSION['error'] = "Missing autos_id";
  header('Location: index.php');
  return;
}

$stmt = $pdo->prepare("SELECT * FROM autos WHERE auto_id = :xyz");
$stmt->execute(array(":xyz" => $_GET['autos_id']));
$row = $stmt->fetch(PDO::FETCH_ASSOC);
if ( $row === false ) {
    $_SESSION['error'] = 'Bad value for autos_id';
    header( 'Location: index.php' ) ;
    return;
}

?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Janta Roy Antor</title>
	<link rel="stylesheet" href="css/bootstrap.min.css">
</head>
<body>
	<nav></nav>
	<div class="container">
		<p>Confirm: Deleting <?php echo $row['make'].' '.$row['model'].' ?'; ?></p>
		<form action="" method="POST">
			<input type="hidden" name="autos_id" value="<?php echo $row['auto_id']; ?>">
			<input type="submit" name="delete" value="Delete">
			<input type="submit" name="cancel" value="Cancel">
		</form>
	</div>
</body>
</html>

